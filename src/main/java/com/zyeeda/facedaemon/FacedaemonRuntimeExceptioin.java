package com.zyeeda.facedaemon;

public class FacedaemonRuntimeExceptioin extends RuntimeException {
	
	private static final long serialVersionUID = 2164980896853594561L;

	public FacedaemonRuntimeExceptioin(){
		super();
	}
	
	public FacedaemonRuntimeExceptioin(String message){
		super(message);
	}
}
