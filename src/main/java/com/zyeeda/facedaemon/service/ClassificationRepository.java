package com.zyeeda.facedaemon.service;

import com.zyeeda.facedaemon.entity.Face;

public interface ClassificationRepository  extends Repository<Face>{
	
	/**
	 * 根据人脸对照片进行分类
	 * 
	 * @param face
	 */
	void classify(Face face);
	

}
