package com.zyeeda.facedaemon.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zyeeda.facedaemon.Constants;
import com.zyeeda.facedaemon.Facedaemon;
import com.zyeeda.facedaemon.entity.Face;
import com.zyeeda.facedaemon.entity.Partition;
import com.zyeeda.facedaemon.entity.Slot;
import com.zyeeda.facedaemon.service.RecognitionRepository;
import com.zyeeda.facedaemon.train.Trainer;

public class RecognitionRepositoryImpl extends BaseFaceRepository implements RecognitionRepository {
	
	Map<String, Trainer> trainers = new HashMap<String, Trainer>();
	
	private boolean locked = false;
	
	@Override
	public void pushSlot(Slot<Face> slot) {
		Partition partition = findOrCreatePartition(slot.getPartition().getName());
		Slot<Face> newSlot = createSlot(partition, slot.getName());
		List<Face>  faces =  slot.getItems();
		for(Face item : faces){
			addItemToSlot(item, newSlot);
		}
	}

	@Override
	public void train() {
		throw new UnsupportedOperationException();
		
	}

	@Override
	public void train(Partition partition) {
		locked = true;
		Trainer trainer = getTrainer(partition);
		trainer.train();
		locked = false;
	}

	@Override
	public Date getLastTrainTime(Partition partition) {
		// TODO
		return null;
	}

	@Override
	public boolean isLocked(Partition partition) {
		return locked;
	}

	@Override
	public String getPath() {
		return Facedaemon.getRepositoryPath() + Constants.REPOSITORY_NAME_SAMPLE;
	}
	
	private Trainer getTrainer(Partition partition){
		Trainer trainer = trainers.get(partition.getName());
		if(trainer==null){
			trainer = new Trainer(this, partition, getPath());
		}
		
		return trainer;
	}
	
	public static void main(String ...strings){
		Facedaemon facedaemon = new Facedaemon();
		facedaemon.setRepositoryPath("/Users/child/Documents/tmp/pictures/");
		facedaemon.setConfidence(0.95f);
		Partition partition = new Partition();
		partition.setName("zyeeda");
		
		
//		facedaemon.getRecognitionRepository().findOrCreateSlot(partition, "123");
//		facedaemon.getRecognitionRepository().train(partition);
//		Slot<Face> slot = new Slot<Face>();
//		slot.setName("123");
//		slot.setPartition(partition);
//		facedaemon.getRecognitionRepository().removeSlot(slot);
		
//		facedaemon.getEngineService().startProcessing();
		
//		List<String> list = new ArrayList<String>();
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen1.jpg"); 
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen10.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen11.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen12.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen13.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen14.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen15.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen16.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen17.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen18.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen19.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen2.jpg"); 
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen20.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen21.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen22.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen23.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen24.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen25.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen26.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen27.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen28.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen29.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen3.jpg"); 
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen30.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen4.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen5.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen6.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen7.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen8.jpg");
//		list.add("/Users/child/Documents/workspace/20141126-luna/facer-opencv/images/gushusen/face-gushusen9.jpg");
//		
//		File file = null;
//		String base64 = null;
//		for(String path : list){
//			file = new File(path);
//			try {
//				base64 = DatatypeConverter.printBase64Binary(FileUtils.readFileToByteArray(file));
//				
//				FileUtils.write(new File(file.getPath() + ".txt"), base64);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
	}

}
