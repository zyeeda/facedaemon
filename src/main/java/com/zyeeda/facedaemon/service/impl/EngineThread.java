package com.zyeeda.facedaemon.service.impl;

import static org.bytedeco.javacpp.opencv_core.cvReleaseImage;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.FileUtils;
import org.bytedeco.javacpp.opencv_objdetect.CvHaarClassifierCascade;

import com.zyeeda.facedaemon.FacedaemonRuntimeExceptioin;
import com.zyeeda.facedaemon.entity.Face;
import com.zyeeda.facedaemon.entity.Image;
import com.zyeeda.facedaemon.entity.Partition;
import com.zyeeda.facedaemon.entity.Photo;
import com.zyeeda.facedaemon.entity.RecognizeResult;
import com.zyeeda.facedaemon.service.ClassificationRepository;
import com.zyeeda.facedaemon.service.EngineService;
import com.zyeeda.facedaemon.service.TransitRepository;

public class EngineThread implements Runnable {

	private static final Logger LOGGER = Logger.getLogger(EngineThread.class.getName());

	private EngineService engineService;
	private TransitRepository transitRepository;
	private ClassificationRepository classificationRepository;
	private MessageService messageService;

	private boolean processingStoped = false;

	private boolean classifyStoped = false;

	public EngineThread(EngineService engineService,
			TransitRepository transitRepository,
			ClassificationRepository classificationRepository,
			CvHaarClassifierCascade classifier, MessageService messageService) {
		this.engineService = engineService;
		this.transitRepository = transitRepository;
		this.classificationRepository = classificationRepository;
		this.messageService = messageService;
	}

	@Override
	public void run() {
		List<Partition> partitions = null;
		List<Photo> photos = null;
		List<Face> faceImages = null;
		RecognizeResult recognizeResult = null;

		//TODO 逻辑提出方法
		while (!processingStoped) {
			partitions = engineService.getPartitions();
			for (Partition partition : partitions) {
				photos = transitRepository.getItemsByPartition(partition);

				try {

					for (Photo photo : photos) {
						if (photo.getImage() == null) {
							transitRepository.removeItem(photo);
							releaseImage(photo);
							continue;
						}
						faceImages = engineService.extractFaces(photo);
						
						if (faceImages.isEmpty()) {
							LOGGER.info("---- no face is found, to be continue....");
						}else{
							for (Face face : faceImages) {
								recognizeResult = engineService.recognizeFace(partition, face);
								if (recognizeResult == null) {
									LOGGER.info("---- recognize fail, to classify....");
									if (!classifyStoped) {
										classificationRepository.classify(face);
									}
								} else {
									String base64Photo = DatatypeConverter.printBase64Binary(FileUtils.readFileToByteArray(photo.getFile()));
									sendMessage(recognizeResult, face.getName(), base64Photo);
									LOGGER.info("---- " + recognizeResult + "is coming...");
								}
								releaseImage(face);
							}
						}
						
						transitRepository.removeItem(photo);
						
						releaseImage(photo);
					}

				} catch (Exception e) {
					e.printStackTrace();
					//TODO do log
				}
			}

		}

	}

	private void releaseImage(Image image){
		if(image.getImage()!=null){
			cvReleaseImage(image.getImage());
			image.setImage(null);
		}
	}
	private void sendMessage(RecognizeResult result, String faceName, String photo) {

		String datetime = faceName.substring(0, 19);

		Date date = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		try {
			date = dateFormat.parse(datetime);
		} catch (ParseException pe) {
			throw new FacedaemonRuntimeExceptioin("could not parse datetime : " + datetime);
		}

		Map<String, Object> message = new HashMap<String, Object>();
		message.put("userName", result.getName());
		message.put("confidence", result.getConfidence());
		message.put("datetime", date);
		message.put("photo", photo);

		this.messageService.getEventBus().post(message);
	}

}
