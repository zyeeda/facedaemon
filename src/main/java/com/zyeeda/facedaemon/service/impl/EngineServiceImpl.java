package com.zyeeda.facedaemon.service.impl;

import static org.bytedeco.javacpp.opencv_core.cvLoad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import org.bytedeco.javacpp.opencv_objdetect.CvHaarClassifierCascade;

import com.zyeeda.facedaemon.Constants;
import com.zyeeda.facedaemon.Facedaemon;
import com.zyeeda.facedaemon.classify.FaceMatcher;
import com.zyeeda.facedaemon.detect.FaceDetector;
import com.zyeeda.facedaemon.entity.Camera;
import com.zyeeda.facedaemon.entity.Face;
import com.zyeeda.facedaemon.entity.Partition;
import com.zyeeda.facedaemon.entity.Photo;
import com.zyeeda.facedaemon.entity.RecognizeResult;
import com.zyeeda.facedaemon.recognize.Recognizer;
import com.zyeeda.facedaemon.service.ClassificationRepository;
import com.zyeeda.facedaemon.service.EngineService;
import com.zyeeda.facedaemon.service.TransitRepository;

public class EngineServiceImpl implements EngineService {

	private static final Logger LOGGER = Logger.getLogger(EngineServiceImpl.class.getName());

	private Map<String, Partition> mapping = new HashMap<String, Partition>();

	private Map<String, Recognizer> recognizers = new HashMap<String, Recognizer>();

	private FaceDetector detector = null;

	private FaceMatcher matcher = new FaceMatcher();

	private boolean classifyStoped = false;

	private TransitRepository transitRepository;

	private ClassificationRepository classificationRepository;

	private Float confidence = 0.9f;

	private MessageService messageService;

	public void setTransitRepository(TransitRepository transitRepository) {
		this.transitRepository = transitRepository;
	}

	public void setClassificationRepository(
			ClassificationRepository classificationRepository) {
		this.classificationRepository = classificationRepository;
	}

	public void setMessageService(MessageService messageService) {
		this.messageService = messageService;
	}

	@Override
	public void associateCamera(Camera camera, Partition parition) {
		mapping.put(camera.getSn(), parition);
		classificationRepository.findOrCreatePartition(parition.getName());
	}

	@Override
	public Partition findPartitionByCamera(Camera camera) {
		return findPartitionByCameraSn(camera.getSn());
	}

	@Override
	public Partition findPartitionByCameraSn(String cameraSn) {
		return mapping.get(cameraSn);
	}

	@Override
	public void startFaceClassification() {
		classifyStoped = false;
	}

	@Override
	public void stopFaceClassification() {
		classifyStoped = true;

	}

	@Override
	public List<Face> extractFaces(Photo photo) {
		return detector.detect(photo);
	}

	@Override
	public double contrastFaces(Face src, Face dest) {
		return matcher.match(src.getImage(), dest.getImage());
	}

	@Override
	public RecognizeResult recognizeFace(Partition partition, Face face) {
		return getRecognizer(partition).recognize(face);
	}

	private Recognizer getRecognizer(Partition partition) {
		Recognizer recognizer = recognizers.get(partition.getName());
		if (recognizer == null) {
			recognizer = new Recognizer(partition, Facedaemon.getRepositoryPath() + Constants.REPOSITORY_NAME_SAMPLE, confidence);
			recognizers.put(partition.getName(), recognizer);
		}
		return recognizer;
	}

	public boolean isClassifyStoped() {
		return classifyStoped;
	}

	@Override
	public void startProcessing() {

		CvHaarClassifierCascade classifier = getClassifier();
		detector = new FaceDetector(classifier);
		EngineThread thread = new EngineThread(this, transitRepository,
				classificationRepository, classifier, messageService);
		ExecutorService executorService = Executors.newSingleThreadExecutor();
		executorService.execute(thread);
		LOGGER.info("engine started...");
	}

	private CvHaarClassifierCascade getClassifier() {
		CvHaarClassifierCascade classifier = null;
		classifier = new CvHaarClassifierCascade(cvLoad(Facedaemon.getRepositoryPath() + "haarcascade_frontalface_alt2.xml"));
		return classifier;
	}

	@Override
	public void stopProcessing() {
		//!TODO
	}

	public List<Partition> getPartitions() {
		return new ArrayList<Partition>(mapping.values());
	}

	public void setConfidence(Float confidence) {
		this.confidence = confidence;
	}

}
