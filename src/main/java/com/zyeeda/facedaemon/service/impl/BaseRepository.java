package com.zyeeda.facedaemon.service.impl;

import static org.bytedeco.javacpp.opencv_highgui.cvLoadImage;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;

import com.zyeeda.facedaemon.Constants;
import com.zyeeda.facedaemon.entity.Image;
import com.zyeeda.facedaemon.entity.Partition;
import com.zyeeda.facedaemon.entity.Slot;
import com.zyeeda.facedaemon.service.Repository;
import com.zyeeda.facedaemon.util.FileHelper;

public abstract class BaseRepository<T extends Image> implements Repository<T> {

	private static final Logger LOGGER = Logger.getLogger(BaseRepository.class.getName());
	
	@Override
	public Partition createPartition(String name) { 
		//FileName Utile
		String path = getPath() + name;
		Partition partition = new Partition();
		File file = new File(path);
		FileHelper.makeDir(file);
		partition.setName(name);
		return partition;
	}

	@Override
	public Partition findPartition(String name) {
		String path = getPath() + name;
		Partition partition = new Partition();
		File file = new File(path);
		if(file.exists()){
			partition.setName(name);
			return partition;
		}else{
			return null;
		}
	}

	@Override
	public Partition findOrCreatePartition(String id) {
		Partition partition = findPartition(id);
		if(partition==null){
			partition = createPartition(id);
		}
		return partition;
	}

	@Override
	public Slot<T> createSlot(Partition partition, String name) {
		String path = getPath() + partition.getName() + "/" + name;
		Slot<T> slot = new Slot<T>();

		FileHelper.makeDir(new File(path));
		slot.setName(name);
		slot.setPartition(partition);
		
		return slot;
	}

	@Override
	public Slot<T> findSlot(Partition partition, String name) {
		return findSlot(partition, name, false);
	}
	
	@Override
	public Slot<T> findSlot(Partition partition, String name, boolean loadImage) {
		String path = getPath() + partition.getName() + "/" + name;
		Slot<T> slot = new Slot<T>();
		File file = new File(path);
		if(file.exists()){
			slot.setName(name);
			slot.setPartition(partition);
			slot.setItems(getItemsBySlot(slot, loadImage));
			
			return slot;
		}else{
			return null;
		}
	}

	@Override
	public Slot<? extends Image> findOrCreateSlot(Partition partition, String name) {
		Slot<? extends Image> slot = findSlot(partition, name);
		if(slot==null){
			slot = createSlot(partition, name);
		}
		return slot;
	}

	@Override
	public List<Slot<T>> getSlotsByPartition(Partition partition) {
		final String path = getPath() + partition.getName();
		List<Slot<T>> slots = new ArrayList<Slot<T>>();
		Slot<T> slot = null;
		File file = new File(path);
		
		File files[] = file.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				File subFile = null;
				subFile = new File(path + "/" + name);
				return subFile.exists()&&subFile.isDirectory();
			}
		});
		
		if(files==null||files.length==0){
			return slots;
		}
		
		int index = 0;
		for(File tmpFile : files){
			slot = new Slot<T>();
			slot.setName(tmpFile.getName());
			slot.setPartition(partition);
			slot.setItems(getItemsBySlot(slot, true, index));
			slots.add(slot);
			
			index++;
		}
		return slots;
	}
	

	@Override
	public List<Slot<T>> getOrderedSlotsByPartition(Partition partition) {
		final String path = getPath() + partition.getName();
		List<Slot<T>> slots = new ArrayList<Slot<T>>();
		Slot<T> slot = null;
		File file = new File(path);
		
		File files[] = file.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				File subFile = null;
				subFile = new File(path + "/" + name);
				return subFile.exists()&&subFile.isDirectory();
			}
		});
		
		if(files==null||files.length==0){
			return slots;
		}
		
		
		Arrays.sort(files, new Comparator<File>() {

			@Override
			public int compare(File o1, File o2) {
				if (o1.listFiles().length < o2.listFiles().length) {
					return 0;
				} else {
					return -1;
				}
			}
			
		});
		
		
		int index = 0;
		for(File tmpFile : files){
			slot = new Slot<T>();
			slot.setName(tmpFile.getName());
			slot.setPartition(partition);
			slot.setItems(getItemsBySlot(slot, true, index));
			slots.add(slot);
			
			index++;
		}
		return slots;
	}

	@Override
	public void removeSlot(Slot<T> slot) {
		String path = getPath() + slot.getPartition().getName() + "/" + slot.getName();
		
		LOGGER.info("removeSlot path = " + path);
		File file = new File(path);
		if(file.exists()){
			FileUtils.deleteQuietly(file);
		}
		
	}

	@Override
	public void renameSlot(Slot<T> slot, String name) {
		String srcPath = getPath() + "/" + slot.getPartition().getName() + "/" + slot.getName();
		String destPath = getPath() + "/" + slot.getPartition().getName() + "/" + name;
		File srcFile = new File(srcPath);
		if(srcFile.exists()){
			srcFile.renameTo(new File(destPath));
		}
		
	}

	@Override
	public void addItem(T item) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addItemToSlot(T item, Slot<T> slot) {
		String path = getPath()  + slot.getPartition().getName() + "/" + slot.getName() + "/" + item.getName();
		File dest = FileHelper.createFile(path);
		item.renameTo(dest);
	}

	@Override
	public List<T> getItemsBySlot(Slot<T> slot) {
		return getItemsBySlot(slot, false);
	}
	
	@Override
	public List<T> getItemsBySlot(Slot<T> slot, boolean loadImage){
		return getItemsBySlot(slot, loadImage, null);
	}
	@Override
	public List<T> getItemsBySlot(Slot<T> slot, boolean loadImage, Integer slotOrder){
		String path = getPath() + slot.getPartition().getName() + "/" + slot.getName();
		List<T> items = new ArrayList<T>();
		File file = new File(path);
		T item = null;
		
		if(file.exists()){
			
			String imageNames[] = file.list(new FilenameFilter() {
				public boolean accept(File dir, String name) {
					return name.endsWith(Constants.EXTENSION_NAME_IMAGE);
				}
			});
			if(imageNames==null||imageNames.length==0){
				return items;
			}
			for(String imageName : imageNames){
				item = createItem(new File(path + "/" + imageName), slotOrder);
				item.setName(imageName);
				item.setSlot(slot);
				
				if(loadImage){
					item.setImage(cvLoadImage(path + "/" + imageName, 0));
				}
				
				items.add(item);
			}
		}
		return items;
	}
	@Override
	public List<T> getItemsByPartition(Partition partition) {
		List<Slot<T>> slots = getSlotsByPartition(partition);
		List<T> items = new ArrayList<T>();
		for(Slot<T> slot : slots){
			items.addAll(slot.getItems());
		}
		return items;
	}

	@Override
	public void removeItemsBySlot(Slot<T> slot) {
		List<T> items = getItemsBySlot(slot);
		for(T item : items){
			item.delete();
		}
	}

	@Override
	public void removeItem(T item) {
		item.delete();
	}
	
	@Override
	public void moveItem(T item, Slot<T> slot) {
		String destPath = getPath() + slot.getPartition().getName() + "/" + slot.getName() + "/" + item.getName();
		File destFile = new File(destPath);
		item.renameTo(destFile);
	}
	
	public void moveItem(T item, Slot<T> slot, Repository<T> repository){
		File dest = new File(repository.getPath() + "/" + slot.getPartition().getName() + "/" + slot.getName() + "/" + item.getName());
		item.renameTo(dest);
	}
	
	protected abstract T createItem(File file, Integer slotOrder);

	@Override
	public Slot<T> moveSlot(Slot<T> slot, Repository<T> repository,	String slotName) {
		Partition partition = repository.findOrCreatePartition(slot.getPartition().getName());
		File destFile = new File(repository.getPath() + "/" + partition.getName() + "/" + slotName);
		LOGGER.info("destFile = " + destFile.getPath());
		FileHelper.makeDir(destFile);
		
		File srcDir = new File(getPath() + slot.getPartition().getName() + "/" + slot.getName());
		LOGGER.info("srcFile = " + srcDir.getPath());
		
		File files[] = srcDir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.endsWith(Constants.EXTENSION_NAME_BASE64) || name.endsWith(Constants.EXTENSION_NAME_IMAGE);
			}
		});
		
		for(File file : files){
			file.renameTo(new File(destFile.getPath() + "/" + file.getName()));
		}
		
		srcDir.delete();
		return repository.findSlot(partition, slotName);
	}
	
}
