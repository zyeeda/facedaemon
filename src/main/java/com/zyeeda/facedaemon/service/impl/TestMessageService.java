package com.zyeeda.facedaemon.service.impl;

import com.google.common.eventbus.Subscribe;
import com.zyeeda.facedaemon.Facedaemon;

public class TestMessageService {

	public TestMessageService() {
		Facedaemon facedaemon = new Facedaemon();
		facedaemon.setRepositoryPath("/Users/child/Documents/tmp/pictures/");
		
		
		//这个是注册eventbus。
		MessageService messageService = facedaemon.getMessageService();
		messageService.getEventBus().register(this);
	}
	
	@Subscribe
	public void processMessage() {
		System.out.println("消息处理接受了～～～");
	}
}
