package com.zyeeda.facedaemon.service.impl;

import static org.bytedeco.javacpp.opencv_core.cvReleaseImage;
import static org.bytedeco.javacpp.opencv_highgui.cvLoadImage;
import static org.bytedeco.javacpp.opencv_highgui.cvSaveImage;
import static org.bytedeco.javacpp.opencv_imgproc.CV_TM_CCOEFF;
import static org.bytedeco.javacpp.opencv_imgproc.CV_TM_CCOEFF_NORMED;
import static org.bytedeco.javacpp.opencv_imgproc.CV_TM_CCORR;
import static org.bytedeco.javacpp.opencv_imgproc.CV_TM_CCORR_NORMED;
import static org.bytedeco.javacpp.opencv_imgproc.CV_TM_SQDIFF;
import static org.bytedeco.javacpp.opencv_imgproc.CV_TM_SQDIFF_NORMED;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.io.FileUtils;
import org.bytedeco.javacpp.opencv_core.IplImage;

import com.zyeeda.facedaemon.Constants;
import com.zyeeda.facedaemon.Facedaemon;
import com.zyeeda.facedaemon.classify.FaceMatcher;
import com.zyeeda.facedaemon.entity.Face;
import com.zyeeda.facedaemon.service.ClassificationRepository;

public class ClassificationRepositoryImpl extends BaseFaceRepository
		implements ClassificationRepository {

	private static final Logger LOGGER = Logger
			.getLogger(ClassificationRepositoryImpl.class.getName());

	private final Map<String, List<CacheSimilarity>> CACHED_REPOSITORY = new HashMap<String, List<CacheSimilarity>>();
	private List<CacheSimilarity> cachedSimilarities;
	
	private FaceMatcher matcher = new FaceMatcher();

	private Long capacity = 1000l;
	private Long slotMaxCapacity = 10l;
	private Long slotMinCapacity = 3L;
	private Long maxIdleTime;
	private Double similarity = 0.95d;
	private Integer algorithm = CV_TM_CCORR_NORMED;

	@Override
	public void classify(Face face) {
			File home = new File(this.getPath(), face.getSlot().getPartition()
					.getName());
			File indexFile = new File(home, Constants.SLOT_SIZE_COUNTER_FILE_NAME);
			File firstSlot = new File(home, "0");
			File firstFace = new File(firstSlot, face.getName());
			if (!firstSlot.exists()) {
				firstSlot.mkdirs();
				
				CACHED_REPOSITORY.put(firstSlot.getPath(), new SortedList<CacheSimilarity>());
				cvSaveImage(firstFace.getPath(), face.getImage());
				try {
					FileUtils.writeStringToFile(indexFile , "0");
					saveImageBase64(firstFace);
				} catch (IOException e) {
					LOGGER.log(Level.SEVERE, e.getMessage());
					e.printStackTrace();
				}
				
			}

			File[] classifies = getSlotsByPath(home);

			// 超过设定的槽位，将启动清理。
			if (classifies.length >= this.capacity) {
				try {
					this.cleanSlot(home, this.slotMinCapacity);
					this.classify(face);
				} catch (IOException e) {
					LOGGER.log(Level.SEVERE, e.getMessage());
					e.printStackTrace();
				}
			} else {
				double result = 0D;
				for (int i = 0; i < classifies.length; i++) {
					
					File[] destes = classifies[i].listFiles(new FilenameFilter() {
						public boolean accept(File dir, String name) {
							return name.endsWith(Constants.EXTENSION_NAME_IMAGE);
						}
					});
					
					if (destes == null || destes.length <= 0) {
						continue;
					}

					File dest = destes[0];
					IplImage iplDest = cvLoadImage(dest.getPath(), 0);
					result = matcher.match(face.getImage(), iplDest);

					LOGGER.info("similaritry result = " + result);

					try {
						if (isBestMatchBySimilarity(this.algorithm, result)) {
							LOGGER.info("dest image path = " + dest.getPath());
							LOGGER.info("source image path = " + face.getName());
							cachedSimilarities = CACHED_REPOSITORY.get(classifies[i].getPath());
							
							if (cachedSimilarities == null) {
								cachedSimilarities = new SortedList<CacheSimilarity>();
							}
							
							CacheSimilarity cache = new CacheSimilarity();
							cache.setFaceName(face.getName());
							cache.setSimilarity(result);
							cachedSimilarities.add(cache);
							File destFace = new File(classifies[i], face.getName());
							cvSaveImage(destFace.getPath(), face.getImage());
							saveImageBase64(destFace);
							
							if (cachedSimilarities.size() >= this.slotMaxCapacity) {
								CacheSimilarity removeItem = cachedSimilarities.remove(0);
								CACHED_REPOSITORY.put(classifies[i].getPath(),
										cachedSimilarities);
								
								//删除多余（超出单槽最大文件数）的照片文件
								File removeImage = new File(classifies[i],
										removeItem.getFaceName());
								removeImage.delete();
								
								//删除多余的（超出单槽最大文件数）base 64 文件
								File removeBase64 = new File(removeImage.getPath() + Constants.EXTENSION_NAME_BASE64);
								removeBase64.delete();
							}

							break;
						}
					} catch (NoMatchMethodException e) {
						LOGGER.log(Level.SEVERE, e.getMessage());
						e.printStackTrace();
					} catch (IOException e) {
						LOGGER.log(Level.SEVERE, e.getMessage());
						e.printStackTrace();
					}

					cvReleaseImage(iplDest);
				}

				try {
					if (isWorstMatchBySimilarity(this.algorithm, result)) {
						String fileName = FileUtils.readFileToString(indexFile);
						long newSlotName = new Long(fileName).longValue() + 1;
						File template = new File(home, new Long(newSlotName).toString());
						template.mkdirs();
						
						CACHED_REPOSITORY.put(template.getPath(), new SortedList<CacheSimilarity>());

						File templateFace = new File(template, face.getName());
						cvSaveImage(templateFace.getPath(), face.getImage());
						FileUtils.writeStringToFile(indexFile, new Long(newSlotName).toString());
						saveImageBase64(templateFace);

					}
				} catch (NumberFormatException e) {
					LOGGER.log(Level.SEVERE, e.getMessage());
					e.printStackTrace();
				} catch (NoMatchMethodException e) {
					LOGGER.log(Level.SEVERE, e.getMessage());
					e.printStackTrace();
				} catch (IOException e) {
					LOGGER.log(Level.SEVERE, e.getMessage());
					e.printStackTrace();
				}
			}

	}

	private static File[] getSlotsByPath(File parent) {
		File[] directories = parent.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) {
				return new File(current, name).isDirectory();
			}
		});

		return directories;
	}

	private boolean isBestMatchBySimilarity(Integer algorithm, Double similarity)
			throws NoMatchMethodException {
		switch (algorithm) {
		case CV_TM_SQDIFF:
		case CV_TM_SQDIFF_NORMED:
			return similarity < this.similarity;
		case CV_TM_CCORR:
		case CV_TM_CCORR_NORMED:
		case CV_TM_CCOEFF:
		case CV_TM_CCOEFF_NORMED:
			return similarity > this.similarity;
		default:
			throw new NoMatchMethodException("no match best similarity method !!!");
		}
	}

	private boolean isWorstMatchBySimilarity(Integer algorithm,
			Double similarity) throws NoMatchMethodException {
//		switch (algorithm) {
//		case CV_TM_SQDIFF | CV_TM_SQDIFF_NORMED:
//			return similarity >= this.similarity;
//		case CV_TM_CCORR | CV_TM_CCORR_NORMED | CV_TM_CCOEFF
//				| CV_TM_CCOEFF_NORMED:
//			return similarity <= this.similarity;
//		default:
//			throw new Throwable("no match method !!!");
			
			
		switch (algorithm) {
			case CV_TM_SQDIFF:
			case CV_TM_SQDIFF_NORMED:
				return similarity >= this.similarity;
			case CV_TM_CCORR:
			case CV_TM_CCORR_NORMED:
			case CV_TM_CCOEFF:
			case CV_TM_CCOEFF_NORMED:
				return similarity <= this.similarity;
			default:
				throw new NoMatchMethodException("no match worst similarity method !!!");
		}
	}

	private File saveImageBase64(File image) throws IOException {
//		byte[] base64Content = Base64.encodeBase64(FileUtils
//				.readFileToByteArray(image));
		String base64Content = DatatypeConverter.printBase64Binary(FileUtils
				.readFileToByteArray(image));

		File dest = new File(image.getPath() + Constants.EXTENSION_NAME_BASE64);
		FileUtils.writeByteArrayToFile(dest, base64Content.getBytes());

		return dest;
	}

	private void cleanSlot(File rootRepo, Long slotMinCapacity)
			throws IOException {
		File[] classifies = getSlotsByPath(rootRepo);
		for (int i = 0; i < classifies.length; i++) {
			if ((new Date().getTime() - classifies[i].lastModified()) >= this.maxIdleTime) {
				File[] files = classifies[i].listFiles();
				if (files.length <= slotMinCapacity) {
					FileUtils.deleteDirectory(classifies[i]);
				}
			}
		}

		classifies = getSlotsByPath(rootRepo);
		if ((classifies.length - 10) >= this.capacity) {
			this.cleanSlot(rootRepo, slotMinCapacity + 1);
		}
	}

	public void setCapacity(Long capacity) {
		this.capacity = capacity;
	}

	public void setSlotMaxCapacity(Long slotMaxCapacity) {
		this.slotMaxCapacity = slotMaxCapacity;
	}

	public void setSlotMinCapacity(Long slotMinCapacity) {
		this.slotMinCapacity = slotMinCapacity;
	}

	public void setMaxIdleTime(Long maxIdleTime) {
		this.maxIdleTime = maxIdleTime;
	}

	public void setSimilarity(Double similarity) {
		this.similarity = similarity;
	}

	public void setAlgorithm(Integer algorithm) {
		this.algorithm = algorithm;
	}

	@Override
	public String getPath() {
		return Facedaemon.getRepositoryPath() + Constants.REPOSITORY_NAME_CLASSIFIER;
	}
	
	class NoMatchMethodException extends Exception {

		private static final long serialVersionUID = 5357285010288321306L;
		
		public NoMatchMethodException(String message) {
			super(message);
		}
	}
}
