package com.zyeeda.facedaemon.service.impl;

public class CacheSimilarity {

	private String faceName;
	private double similarity;
	
	public String getFaceName() {
		return faceName;
	}
	public void setFaceName(String faceName) {
		this.faceName = faceName;
	}
	public double getSimilarity() {
		return similarity;
	}
	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}
	
	
}
