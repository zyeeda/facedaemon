package com.zyeeda.facedaemon.service.impl;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import com.zyeeda.facedaemon.Constants;
import com.zyeeda.facedaemon.FacedaemonRuntimeExceptioin;
import com.zyeeda.facedaemon.entity.Face;
import com.zyeeda.facedaemon.entity.Slot;
import com.zyeeda.facedaemon.util.FileHelper;

public abstract class BaseFaceRepository extends BaseRepository<Face>{

	@Override
	public Face findItem(Slot<Face> slot, String name) {
		String path = getPath() + slot.getPartition().getName() + "/" + slot.getName() +"/" + name;
		Face face = createItem(new File(path), null);
		face.setName(name);
		face.setSlot(slot);
		return face;
	}

	
	@Override
	protected Face createItem(File file, Integer slotOrder) {
		Face face = new Face();
		face.setFile(file);
		try {
			face.setFileBase64(FileUtils.readFileToString(FileHelper.getBase64ImageFile(file)));
		} catch (IOException e) {
			throw new FacedaemonRuntimeExceptioin("read file '" + file + "" + Constants.EXTENSION_NAME_BASE64+ "' fail.");
		}
		face.setSlotOrder(slotOrder);
		return face;
	}

}
