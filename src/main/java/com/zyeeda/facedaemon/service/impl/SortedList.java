package com.zyeeda.facedaemon.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SortedList<E extends CacheSimilarity> extends ArrayList<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4617789737105226115L;

	@Override
	public boolean add(E e) {
		super.add(e);
		Collections.sort(this, new Comparator<CacheSimilarity>() {

			@Override
			public int compare(CacheSimilarity o1, CacheSimilarity o2) {
				if (o1.getSimilarity() > o2.getSimilarity()) {
					return 0;
				} 
				return -1;
			}
		});
		return true;
	}
}
