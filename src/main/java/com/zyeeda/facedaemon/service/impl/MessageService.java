package com.zyeeda.facedaemon.service.impl;

import java.util.concurrent.Executors;

import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;

public class MessageService {

	private EventBus bus = null;
	
	public MessageService(boolean isAsync) {
		if (isAsync) {
			this.bus = new AsyncEventBus(Executors.newFixedThreadPool(1));
		} else {
			this.bus = new EventBus();
		}
	}
	
	public EventBus getEventBus() {
		return this.bus;
	}
//	public void publish(Map<String, Object> message) {
//		bus.post(message);
//	}
//	
//	public void subscribe(MessageSubscriber subscriber) {
//		this.bus.register(subscriber);
//	}
	
}
