package com.zyeeda.facedaemon.service.impl;

import java.io.File;

import com.zyeeda.facedaemon.Constants;
import com.zyeeda.facedaemon.Facedaemon;
import com.zyeeda.facedaemon.entity.Partition;
import com.zyeeda.facedaemon.entity.Photo;
import com.zyeeda.facedaemon.entity.Slot;
import com.zyeeda.facedaemon.service.EngineService;
import com.zyeeda.facedaemon.service.TransitRepository;

public class TransitRepositoryImpl extends BaseRepository<Photo> implements TransitRepository {

	private EngineService engineService;
	
	public void setEngineService(EngineService engineService) {
		this.engineService = engineService;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void classify(Photo photo) {
		Partition partition = engineService.findPartitionByCamera(photo.getCamera());
		partition = findOrCreatePartition(partition.getName());
		Slot<Photo> slot = (Slot<Photo>)findOrCreateSlot(partition, photo.getCamera().getSn());
		addItemToSlot(photo, slot);
	}

	@Override
	public String getPath() {
		return Facedaemon.getRepositoryPath() + Constants.REPOSITORY_NAME_ORIGIN;
	}

	@Override
	protected Photo createItem(File file, Integer slotOrder) {
		Photo photo = new Photo();
		photo.setFile(file);
		return photo;
	}

	@Override
	public Photo findItem(Slot<Photo> slot, String name) {
		String path = getPath() + slot.getPartition().getName() + "/" + slot.getName() +"/" + name;
		Photo photo = createItem(new File(path), null);
		photo.setName(name);
		photo.setSlot(slot);
		return photo;
	}
}