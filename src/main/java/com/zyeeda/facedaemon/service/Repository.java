package com.zyeeda.facedaemon.service;

import java.util.List;

import com.zyeeda.facedaemon.entity.Image;
import com.zyeeda.facedaemon.entity.Partition;
import com.zyeeda.facedaemon.entity.Slot;

public interface Repository<T extends Image> {
	
	/**
	 * 创建分区及对应的文件夹
	 * 
	 * @param id
	 * @return
	 */
	Partition createPartition(String id);
	
	/**
	 * 根据 name 查找分区，不存在的情况下返回空
	 * 
	 * @param name
	 * @return
	 */
	Partition findPartition(String name);
	
	/**
	 * 根据 id 查找分区，如果不存在则创建
	 * 
	 * @param name
	 * @return
	 */
	Partition findOrCreatePartition(String name);
	Slot<T> createSlot(Partition partition, String name);
	
	/**
	 * 在分区下寻找槽位, 不加载槽内的图片（face 或 photo）
	 * 
	 * @param partition
	 * @param name
	 * @return
	 */
	Slot<T> findSlot(Partition partition, String name);
	Slot<T> findSlot(Partition partition, String name, boolean loadImage);
	
	/**
	 * 在分区下，根据名称查找槽位，如果不存在则创建
	 * 
	 * @param partition
	 * @param name
	 * @return
	 */
	Slot<? extends Image> findOrCreateSlot(Partition partition, String name);
	
	/**
	 * 根据 slot 及 item name 查找 item
	 * 
	 * @param slot
	 * @param name
	 * @return
	 */
	T findItem(Slot<T> slot, String name);
	
	/**
	 * 根据分区获取槽位, slot 内的 item 加载图片
	 * 
	 * @param partition
	 * @return
	 */
	List<Slot<T>> getSlotsByPartition(Partition partition);
	
	/**
	 * 根据分区获取槽位，并按槽内的 face 多少倒序排序， slot 内的 item 不加载图片
	 * 
	 * @param partition
	 * @return
	 */
	List<Slot<T>> getOrderedSlotsByPartition(Partition partition);

	void removeSlot(Slot<T> slot);
	void renameSlot(Slot<T> slot, String name);
	void addItem(T item);
	void addItemToSlot(T item, Slot<T> slot);
	
	/**
	 * 获取槽位下的所有元素, item 内不加载图片
	 * 
	 * @param slot
	 * @return
	 */
	List<T> getItemsBySlot(Slot<T> slot);
	
	/**
	 * 获取槽位下的所有元素
	 * 
	 * @param slot
	 * @param loadImage ，true item 加载图片，否则不加载
	 * @return
	 */
	List<T> getItemsBySlot(Slot<T> slot, boolean loadImage);
	
	/**
	 * 获取槽位下的所有元素
	 * 
	 * @param slot
	 * @param loadImage ，true item 加载图片，否则不加载
	 * @return
	 */
	List<T> getItemsBySlot(Slot<T> slot, boolean loadImage, Integer slotOrder);
	
	/**
	 * 获取分区下的所有元素
	 * 
	 * @param partition
	 * @return
	 */
	List<T> getItemsByPartition(Partition partition);
	
	/**
	 * 删除指定元素，并移除对应文件
	 * 
	 * @param item
	 */
	void removeItem(T item);
	
	/**
	 * 删除槽位下的所有元素，但不删除槽位
	 * 
	 * @param slot
	 */
	void removeItemsBySlot(Slot<T> slot);
	
	/**
	 * 将元素移动到指定槽位
	 * 
	 * @param item
	 * @param slot
	 */
	void moveItem(T item, Slot<T> slot);
	
	/**
	 * 将元素移动到指定仓库的指定槽位下
	 *
	 * @param item
	 * @param slot
	 * @param repository
	 * void
	 *
	 * @author child
	 * @date 2014年11月27日
	 *
	 */
	void moveItem(T item, Slot<T> slot, Repository<T> repository);
	
	Slot<T> moveSlot(Slot<T> slot, Repository<T> repository, String slotName);
	
	String getPath();
}
