package com.zyeeda.facedaemon.service;

import com.zyeeda.facedaemon.entity.Photo;

public interface TransitRepository extends Repository<Photo> {
	
	/**
	 * 根据拍摄照片的相机序列号将照片归类
	 * 
	 * @param photo
	 */
	void classify(Photo photo);
}
