package com.zyeeda.facedaemon.service;

import java.util.List;

import com.zyeeda.facedaemon.entity.Camera;
import com.zyeeda.facedaemon.entity.Face;
import com.zyeeda.facedaemon.entity.Partition;
import com.zyeeda.facedaemon.entity.Photo;
import com.zyeeda.facedaemon.entity.RecognizeResult;

public interface EngineService {
	/**
	 * 将摄像头与分区关联
	 * 
	 * @param camera
	 * @param parition
	 */
	void associateCamera(Camera camera, Partition parition);
	Partition findPartitionByCamera(Camera camera);
	Partition findPartitionByCameraSn(String cameraSn);
	void startFaceClassification();
	void stopFaceClassification();
	
	/**
	 * 抽取面孔
	 * 
	 * @param photo
	 * @return
	 */
	List<Face> extractFaces(Photo photo);
	
	/**
	 * 对比两个面孔的相似度
	 * 
	 * @param src
	 * @param dest
	 * @return
	 */
	double contrastFaces(Face src, Face dest);
	
	/**
	 * 人脸识别
	 * 
	 * @param partition
	 * @param face
	 * @return 
	 */
	RecognizeResult recognizeFace(Partition partition, Face face);
	
	// enable disable
	public boolean isClassifyStoped();
	
	public void startProcessing();
	
	public void stopProcessing();
	
	public List<Partition> getPartitions();
}
