package com.zyeeda.facedaemon.service;

import java.util.Date;

import com.zyeeda.facedaemon.entity.Face;
import com.zyeeda.facedaemon.entity.Partition;
import com.zyeeda.facedaemon.entity.Slot;

public interface RecognitionRepository extends Repository<Face> {
	
	/**
	 * 将槽位放置在此库中
	 * 
	 * @param slot
	 */
	void pushSlot(Slot<Face> slot);
	
	/**
	 * 根据此库中的数据进行训练
	 * 
	 */
	void train();
	
	/**
	 * 训练指定分区下的面孔
	 * 
	 * @param partition
	 */
	void train(Partition partition);
	
	Date getLastTrainTime(Partition partition);
	
	boolean isLocked(Partition partition);
//	List<TrainHistory> getTrainHistory(Partition partition);
}
