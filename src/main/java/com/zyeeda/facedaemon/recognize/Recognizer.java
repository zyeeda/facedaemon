package com.zyeeda.facedaemon.recognize;

import static org.bytedeco.javacpp.helper.opencv_legacy.cvEigenDecomposite;
import static org.bytedeco.javacpp.opencv_core.CV_STORAGE_READ;
import static org.bytedeco.javacpp.opencv_core.cvOpenFileStorage;
import static org.bytedeco.javacpp.opencv_core.cvReadByName;
import static org.bytedeco.javacpp.opencv_core.cvReadIntByName;
import static org.bytedeco.javacpp.opencv_core.cvReadStringByName;
import static org.bytedeco.javacpp.opencv_core.cvReleaseFileStorage;
import static org.bytedeco.javacpp.opencv_objdetect.CV_HAAR_SCALE_IMAGE;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.bytedeco.javacpp.FloatPointer;
import org.bytedeco.javacpp.Pointer;
import org.bytedeco.javacpp.opencv_core.CvFileStorage;
import org.bytedeco.javacpp.opencv_core.CvMat;
import org.bytedeco.javacpp.opencv_core.IplImage;

import com.zyeeda.facedaemon.entity.Face;
import com.zyeeda.facedaemon.entity.Partition;
import com.zyeeda.facedaemon.entity.RecognizeResult;

public class Recognizer {

	//引用 origin 换 logger
	private static final Logger LOGGER = Logger.getLogger(Recognizer.class.getName());

	public String RESOURCE_TRAINED_DATA = "trained-data.xml";

	float confidence;

	private Partition partition;
	private String path;

	// 训练的脸的数量
	private int nTrainFaces = 0;
	// 特征数量
	private int nEigens = 0;

	private CvMat projectedTrainFaceMat;
	private CvMat trainPersonNumMat;
	private List<String> personNames = new ArrayList<String>();

	private IplImage[] eigenVects;
	private IplImage avgTrainImg;

	public Recognizer(Partition partition, String path, float confidence) {
		this.partition = partition;
		this.path = path;
		this.confidence = confidence;
		loadTrainedData();
	}

	public RecognizeResult recognize(Face face) {
		return recognize(face, 0 | CV_HAAR_SCALE_IMAGE);
	}

	public RecognizeResult recognize(Face face, int flag) {
		float confidence_now = 0.0f;
		int nearest = 0;
		int iNearest = 0;

		if (trainPersonNumMat == null) {
			return null;
		}
		float[] projectedTestFace = new float[nEigens];
		cvEigenDecomposite(face.getImage(), nEigens, eigenVects, 0, null,
				avgTrainImg, projectedTestFace);

		final FloatPointer pConfidence = new FloatPointer(confidence_now);
		iNearest = findNearestNeighbor(projectedTestFace, new FloatPointer(pConfidence));
		confidence_now = pConfidence.get();
		nearest = trainPersonNumMat.data_i().get(iNearest);
		String personId = personNames.get(nearest);
		LOGGER.info("-------############## ----------" + personId + " confidence is :" + confidence_now);
		if (confidence_now > confidence) {
			RecognizeResult result = new RecognizeResult();
			result.setName(personId);
			result.setConfidence(confidence_now);
			return result;
		}
		
		return null;
	}

	private void loadTrainedData() {

		CvFileStorage storage;
		Pointer pointer = null;
		int totalPersons = 0;
		String trainedDataFilePath = path + partition.getName() + "/" + RESOURCE_TRAINED_DATA;
		storage = cvOpenFileStorage(trainedDataFilePath , null, CV_STORAGE_READ, null);
		if (storage == null) {
			return;
		}

		totalPersons = cvReadIntByName(storage, null, "nPersons", 0);

		String personName;
		personNames.clear();

		for (int i = 0; i < totalPersons; i++) {
			String varname = "personName_" + (i + 1);
			personName = cvReadStringByName(storage, null, varname, "");
			personNames.add(personName);
		}
		LOGGER.info("person names: " + personNames);

		nEigens = cvReadIntByName(storage, null, "nEigens", 0);
		nTrainFaces = cvReadIntByName(storage, null, "nTrainFaces", 0);

		pointer = cvReadByName(storage, null, "trainPersonNumMat");
		trainPersonNumMat = new CvMat(pointer);

		pointer = cvReadByName(storage, null, "projectedTrainFaceMat");
		projectedTrainFaceMat = new CvMat(pointer);

		pointer = cvReadByName(storage, null, "avgTrainImg");
		avgTrainImg = new IplImage(pointer);

		eigenVects = new IplImage[nTrainFaces];
		for (int i = 0; i <= nEigens; i++) {
			String varname = "eigenVect_" + i;
			pointer = cvReadByName(storage, null, varname);
			eigenVects[i] = new IplImage(pointer);
		}

		cvReleaseFileStorage(storage);

		LOGGER.info("Trained data loaded (" + nTrainFaces + " trained images of " + totalPersons + " people)");

	}

	private int findNearestNeighbor(float projectedTestFace[],
			FloatPointer confidencePointer) {
		double leastDistSq = Double.MAX_VALUE;
		int iNearest = 0;

		for (int iTrain = 0; iTrain < nTrainFaces; iTrain++) {
			double distSq = 0;
			for (int i = 0; i < nEigens; i++) {
				float projectedTrainFaceDistance = (float) projectedTrainFaceMat.get(iTrain, i);
				float d_i = projectedTestFace[i] - projectedTrainFaceDistance;
				distSq += d_i * d_i;
			}

			if (distSq < leastDistSq) {
				leastDistSq = distSq;
				iNearest = iTrain;
			}
		}

		float confidenceTemp = (float) (1.0f - Math.sqrt(leastDistSq / (float) (nTrainFaces * nEigens)) / 255.0f);
		confidencePointer.put(confidenceTemp);

		return iNearest;
	}

}
