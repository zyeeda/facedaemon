package com.zyeeda.facedaemon.detect;

import static org.bytedeco.javacpp.helper.opencv_objdetect.cvHaarDetectObjects;
import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U;
import static org.bytedeco.javacpp.opencv_core.cvClearMemStorage;
import static org.bytedeco.javacpp.opencv_core.cvCloneImage;
import static org.bytedeco.javacpp.opencv_core.cvCopy;
import static org.bytedeco.javacpp.opencv_core.cvCreateImage;
import static org.bytedeco.javacpp.opencv_core.cvGetSeqElem;
import static org.bytedeco.javacpp.opencv_core.cvGetSize;
import static org.bytedeco.javacpp.opencv_core.cvReleaseImage;
import static org.bytedeco.javacpp.opencv_core.cvResetImageROI;
import static org.bytedeco.javacpp.opencv_core.cvSetImageROI;
import static org.bytedeco.javacpp.opencv_core.cvSize;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.CV_INTER_AREA;
import static org.bytedeco.javacpp.opencv_imgproc.CV_INTER_LINEAR;
import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;
import static org.bytedeco.javacpp.opencv_imgproc.cvEqualizeHist;
import static org.bytedeco.javacpp.opencv_imgproc.cvResize;
import static org.bytedeco.javacpp.opencv_objdetect.CV_HAAR_SCALE_IMAGE;

import java.util.ArrayList;
import java.util.List;

import org.bytedeco.javacpp.opencv_core.CvMemStorage;
import org.bytedeco.javacpp.opencv_core.CvRect;
import org.bytedeco.javacpp.opencv_core.CvSeq;
import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.opencv_objdetect.CvHaarClassifierCascade;

import com.zyeeda.facedaemon.Constants;
import com.zyeeda.facedaemon.entity.Face;
import com.zyeeda.facedaemon.entity.Photo;

/**
 * 检测图片中是否包含人脸
 *
 * @author child
 * @date 2014年11月27日
 *
 */

public class FaceDetector{
	private CvHaarClassifierCascade classifier;
	public FaceDetector(CvHaarClassifierCascade classifier){
		this.classifier = classifier;
	}
	
	public List<Face> detect(Photo image){
		return detect(image, 0 | CV_HAAR_SCALE_IMAGE);
	}
	public List<Face> detect(Photo photo, int flag){
		List<Face> faces = new ArrayList<Face>();
		
	 	IplImage greyImg = null;
	 	IplImage faceImg = null;
	 	IplImage sizedImg = null;
	 	IplImage equalizedImg = null;

	 	CvRect cvRect ;
	 	greyImg = cvCreateImage(cvGetSize(photo.getImage()), IPL_DEPTH_8U, 1);
		greyImg = convertImageToGreyscale(photo.getImage());
		
		CvMemStorage memStorage = CvMemStorage.create();

		CvSeq sign = cvHaarDetectObjects(greyImg, classifier, memStorage, 1.1, 3, flag);
		cvClearMemStorage(memStorage);
		
		if(sign.total() > 0) {
			Face face = null;
			for(int i = 0 ;i<sign.total();i++) {
				cvRect = new CvRect(cvGetSeqElem(sign, i));
				faceImg = cropImage(greyImg, cvRect);
				sizedImg = resizeImage(faceImg);
				equalizedImg = cvCreateImage(cvGetSize(sizedImg), 8, 1);
				cvEqualizeHist(sizedImg, equalizedImg);
				
				face = new Face();
				face.setName(photo.getName().replace(Constants.EXTENSION_NAME_IMAGE, "") + "-" + i + Constants.EXTENSION_NAME_IMAGE);
				face.setSlot(photo.getSlot());
				face.setImage(equalizedImg);
				
				faces.add(face);
			}
			
			cvReleaseImage(greyImg);
		  	cvReleaseImage(faceImg);
		  	cvReleaseImage(sizedImg);
		}
		return faces;
	}

	private IplImage convertImageToGreyscale(IplImage imageSrc) {
		IplImage imageGrey;
		if (imageSrc.nChannels()==3) {
			imageGrey = cvCreateImage( cvGetSize(imageSrc), IPL_DEPTH_8U, 1);
			cvCvtColor( imageSrc, imageGrey, CV_BGR2GRAY );
		} else {
			imageGrey = cvCloneImage(imageSrc);
		}
		return imageGrey;
	 }

	private IplImage resizeImage(IplImage origImg) {
	  	IplImage outImg = null;
	  	outImg = cvCreateImage(cvSize(50, 50), origImg.depth(), origImg.nChannels());
	  	if (50 > origImg.width() && 50 > origImg.height()) {
	  		cvResetImageROI((IplImage)origImg);
	  		cvResize(origImg, outImg, CV_INTER_LINEAR);
	  	}
	  	else {
	  		cvResetImageROI((IplImage)origImg);
	  		cvResize(origImg, outImg, CV_INTER_AREA);
	  	}

	  	return outImg;
	}

	private IplImage cropImage(IplImage img, CvRect region) {
	  	IplImage imageTmp;
	  	IplImage imageRGB;

	  	if (img.depth() != IPL_DEPTH_8U) {
	  		return null;
	  	}
	  	imageTmp = cvCreateImage(cvGetSize(img), IPL_DEPTH_8U, img.nChannels());
	  	cvCopy(img, imageTmp);
	  	cvSetImageROI(imageTmp, region);
	  	imageRGB = cvCreateImage(cvSize(region.width(),region.height()), IPL_DEPTH_8U, img.nChannels());
	  	cvCopy(imageTmp, imageRGB);
	    cvReleaseImage(imageTmp);

	  	return imageRGB;
	}

}
