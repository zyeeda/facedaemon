package com.zyeeda.facedaemon;

public class Constants {
	
	//TODO 注释
	public static final String EXTENSION_NAME_BASE64 = ".txt";
	
	public static final String EXTENSION_NAME_IMAGE = ".jpg";
	
	public static final String SLOT_SIZE_COUNTER_FILE_NAME = "index";
	
	//TODO 与 repository 一致
	public static final String REPOSITORY_NAME_ORIGIN = "origin/";
	
	public static final String REPOSITORY_NAME_CLASSIFIER = "classifier/";
	
	public static final String REPOSITORY_NAME_SAMPLE = "sample/";
	
}
