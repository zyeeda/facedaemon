package com.zyeeda.facedaemon;

import java.io.File;

import com.zyeeda.facedaemon.service.ClassificationRepository;
import com.zyeeda.facedaemon.service.EngineService;
import com.zyeeda.facedaemon.service.RecognitionRepository;
import com.zyeeda.facedaemon.service.TransitRepository;
import com.zyeeda.facedaemon.service.impl.ClassificationRepositoryImpl;
import com.zyeeda.facedaemon.service.impl.EngineServiceImpl;
import com.zyeeda.facedaemon.service.impl.MessageService;
import com.zyeeda.facedaemon.service.impl.RecognitionRepositoryImpl;
import com.zyeeda.facedaemon.service.impl.TransitRepositoryImpl;
import com.zyeeda.facedaemon.util.FileHelper;

public class Facedaemon {
	
	private EngineService engineService; 
	
	private TransitRepository transitRepository;
	
	private ClassificationRepository classificationRepository;
	
	private RecognitionRepository recognitionRepository;
	
	private MessageService messageService;
	
	private static String repositoryPath;
	
	//TODO
	private Float confidence;
	
	//TODO 移到 构造方法中， 或内部方法
	public static void init(){
		if(repositoryPath==null||repositoryPath.trim().isEmpty()){
			throw new FacedaemonRuntimeExceptioin("repositoryPath should be setted.");
		}
		if(!repositoryPath.endsWith("/")){
			repositoryPath = repositoryPath + "/";
		}
		
		// Common util
		FileHelper.makeDir(new File(repositoryPath));
		FileHelper.makeDir(new File(repositoryPath + Constants.REPOSITORY_NAME_ORIGIN));
		FileHelper.makeDir(new File(repositoryPath + Constants.REPOSITORY_NAME_ORIGIN + "temp/"));
		FileHelper.makeDir(new File(repositoryPath + Constants.REPOSITORY_NAME_CLASSIFIER));
		FileHelper.makeDir(new File(repositoryPath + Constants.REPOSITORY_NAME_SAMPLE));
		
	}
	
	public Facedaemon(){
		transitRepository = new TransitRepositoryImpl();
		classificationRepository = new ClassificationRepositoryImpl();
		recognitionRepository = new RecognitionRepositoryImpl();
		engineService = new EngineServiceImpl();
		messageService = new MessageService(true);
		
		//TODO 构造方法
		((EngineServiceImpl)engineService).setClassificationRepository(classificationRepository);
		((EngineServiceImpl)engineService).setTransitRepository(transitRepository);
		if(confidence!=null){
			((EngineServiceImpl)engineService).setConfidence(confidence);
		}
		((EngineServiceImpl)engineService).setMessageService(messageService);
		
		((TransitRepositoryImpl)transitRepository).setEngineService(engineService);
	}

	public TransitRepository getTransitRepository() {
		return transitRepository;
	}

	public ClassificationRepository getClassificationRepository() {
		return classificationRepository;
	}

	public RecognitionRepository getRecognitionRepository() {
		return recognitionRepository;
	}

	public EngineService getEngineService() {
		return engineService;
	}

	public static String getRepositoryPath() {
		return repositoryPath;
	}
	
	public MessageService getMessageService() {
		return this.messageService;
	}

	public void setRepositoryPath(String repositoryPath) {
		Facedaemon.repositoryPath = repositoryPath;
	}

	public void setConfidence(Float confidence) {
		this.confidence = confidence;
	}
	
}
