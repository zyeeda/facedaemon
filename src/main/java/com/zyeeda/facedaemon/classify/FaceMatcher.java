package com.zyeeda.facedaemon.classify;

import static org.bytedeco.javacpp.opencv_core.cvCreateImage;
import static org.bytedeco.javacpp.opencv_core.cvMinMaxLoc;
import static org.bytedeco.javacpp.opencv_core.cvReleaseImage;
import static org.bytedeco.javacpp.opencv_highgui.cvLoadImage;
import static org.bytedeco.javacpp.opencv_imgproc.CV_TM_CCORR_NORMED;
import static org.bytedeco.javacpp.opencv_imgproc.cvMatchTemplate;

import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.IplImage;

public class FaceMatcher {

    public double match(IplImage sourceImage, IplImage templateImage, Integer algorithm) {
        IplImage result = cvCreateImage(opencv_core.cvSize(
                templateImage.width() - sourceImage.width() + 1,
                templateImage.height() - sourceImage.height() + 1),
                opencv_core.IPL_DEPTH_32F, 1);
        
        opencv_core.cvZero(result);
        cvMatchTemplate(sourceImage, templateImage, result, algorithm);
        double[] minVal = new double[2];
        double[] maxVal = new double[2];

        cvMinMaxLoc(result, minVal, maxVal, null, null, null);
        cvReleaseImage(result);
        return maxVal[0];
    }
    public double match(IplImage sourceImage, IplImage templateImage) {
    	return match(sourceImage, templateImage, CV_TM_CCORR_NORMED);
    }
    public static void main(String ... args) {

//        0 CV_TM_SQDIFF 平方差匹配法，最好的匹配为0，值越大匹配越差
//        1 CV_TM_SQDIFF_NORMED 归一化平方差匹配法
//        2 CV_TM_CCORR 相关匹配法，采用乘法操作，数值越大表明匹配越好
//        3 CV_TM_CCORR_NORMED 归一化相关匹配法
//        4 CV_TM_CCOEFF 相关系数匹配法，最好的匹配为1，-1表示最差的匹配
//        5 CV_TM_CCOEFF_NORMED 归一化相关系数匹配法
    	
    	FaceMatcher matcher = new FaceMatcher();
    	IplImage source = cvLoadImage("/Users/child/Documents/tmp/photos/faces/0-p-2014-11-12-2-24-48-653.jpg");
    	IplImage template = cvLoadImage("/Users/child/Documents/tmp/photos/faces/0-p-2014-11-12-2-25-4-733.jpg");
    	double result = matcher.match(source, template);
    	System.out.println("result = " + result);
    }
}