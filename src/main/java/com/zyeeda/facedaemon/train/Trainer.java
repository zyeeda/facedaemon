package com.zyeeda.facedaemon.train;

import static org.bytedeco.javacpp.helper.opencv_legacy.cvCalcEigenObjects;
import static org.bytedeco.javacpp.helper.opencv_legacy.cvEigenDecomposite;
import static org.bytedeco.javacpp.opencv_core.CV_32FC1;
import static org.bytedeco.javacpp.opencv_core.CV_32SC1;
import static org.bytedeco.javacpp.opencv_core.CV_L1;
import static org.bytedeco.javacpp.opencv_core.CV_STORAGE_WRITE;
import static org.bytedeco.javacpp.opencv_core.CV_TERMCRIT_ITER;
import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_32F;
import static org.bytedeco.javacpp.opencv_core.IPL_DEPTH_8U;
import static org.bytedeco.javacpp.opencv_core.cvConvertScale;
import static org.bytedeco.javacpp.opencv_core.cvCopy;
import static org.bytedeco.javacpp.opencv_core.cvCreateImage;
import static org.bytedeco.javacpp.opencv_core.cvCreateMat;
import static org.bytedeco.javacpp.opencv_core.cvMinMaxLoc;
import static org.bytedeco.javacpp.opencv_core.cvNormalize;
import static org.bytedeco.javacpp.opencv_core.cvOpenFileStorage;
import static org.bytedeco.javacpp.opencv_core.cvRect;
import static org.bytedeco.javacpp.opencv_core.cvReleaseFileStorage;
import static org.bytedeco.javacpp.opencv_core.cvReleaseImage;
import static org.bytedeco.javacpp.opencv_core.cvResetImageROI;
import static org.bytedeco.javacpp.opencv_core.cvSetImageROI;
import static org.bytedeco.javacpp.opencv_core.cvSize;
import static org.bytedeco.javacpp.opencv_core.cvTermCriteria;
import static org.bytedeco.javacpp.opencv_core.cvWrite;
import static org.bytedeco.javacpp.opencv_core.cvWriteInt;
import static org.bytedeco.javacpp.opencv_core.cvWriteString;
import static org.bytedeco.javacpp.opencv_highgui.cvSaveImage;
import static org.bytedeco.javacpp.opencv_legacy.CV_EIGOBJ_NO_CALLBACK;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.bytedeco.javacpp.DoublePointer;
import org.bytedeco.javacpp.FloatPointer;
import org.bytedeco.javacpp.opencv_core.CvFileStorage;
import org.bytedeco.javacpp.opencv_core.CvMat;
import org.bytedeco.javacpp.opencv_core.CvPoint;
import org.bytedeco.javacpp.opencv_core.CvRect;
import org.bytedeco.javacpp.opencv_core.CvSize;
import org.bytedeco.javacpp.opencv_core.CvTermCriteria;
import org.bytedeco.javacpp.opencv_core.IplImage;

import com.zyeeda.facedaemon.entity.Face;
import com.zyeeda.facedaemon.entity.Partition;
import com.zyeeda.facedaemon.entity.Slot;
import com.zyeeda.facedaemon.service.RecognitionRepository;

public class Trainer {

	private static final Logger LOGGER = Logger.getLogger(Trainer.class.getName());

	public String RESOURCE_TRAINED_DATA = "trained-data.xml";
	private RecognitionRepository recognitionRepository;
	private Partition partition;
	private String path;
	private int personNumber = 0;
	// 需要训练的脸的数量
	private int nTrainFaces = 0;
	private int nEigens = 0;

	private CvMat personNumTruthMat;
	private CvMat eigenValMat;
	private CvMat projectedTrainFaceMat;
	private List<String> personNames = new ArrayList<String>();

	private List<IplImage> eigenVects;
	private List<IplImage> faceImages4Train;
	private IplImage avgTrainImg;

	public Trainer(RecognitionRepository recognitionRepository, Partition partition, String path) {
		this.recognitionRepository = recognitionRepository;
		this.partition = partition;
		this.path = path;
	}

	public void train() {
		faceImages4Train = loadTrainingImages();
		nTrainFaces = faceImages4Train.size();

		if (nTrainFaces < 1) {
			return;
		}

		doPCA();
		projectedTrainFaceMat = cvCreateMat(nTrainFaces, nEigens, CV_32FC1);

		for (int i = 0; i < nTrainFaces; i++) {
			for (int j = 0; j < nEigens; j++) {
				projectedTrainFaceMat.put(i, j, 0.0);
			}
		}

		LOGGER.info("created projectedTrainFaceMat with " + nTrainFaces + " (nTrainFaces) rows and " + nEigens + " (nEigens) columns");

		final org.bytedeco.javacpp.FloatPointer floatPointer = new FloatPointer(nEigens);
		for (int i = 0; i < nTrainFaces; i++) {
			cvEigenDecomposite(
					faceImages4Train.get(i), 
					nEigens,
					convertListToArray(eigenVects), 
					0, 
					null, 
					avgTrainImg,
					floatPointer
					);

			for (int j = 0; j < nEigens; j++) {
				projectedTrainFaceMat.put(i, j, floatPointer.get(j));
			}
		}

		storeTrainedData();
		storeEigenfaceImages();
	}

	private List<IplImage> loadTrainingImages() {

		List<IplImage> faceImages = new ArrayList<IplImage>();
		List<Face> faces = recognitionRepository.getItemsByPartition(partition);

		personNumTruthMat = cvCreateMat(1, faces.size(), CV_32SC1);

		personNames = getPersonNames();
		personNumber = personNames.size();

		int i = 0;
		for (Face face : faces) {
			personNumTruthMat.put(0, i, face.getSlotOrder());
			faceImages.add(face.getImage());
			i++;
		}

		return faceImages;
	}

	private List<String> getPersonNames() {
		List<Slot<Face>> slots = recognitionRepository.getSlotsByPartition(partition);
		List<String> personNames = new ArrayList<String>(slots.size());
		for (Slot<Face> slot : slots) {
			personNames.add(slot.getName());
		}

		return personNames;
	}

	// 主成分分析
	private void doPCA() {
		int i;
		CvTermCriteria calcLimit;
		CvSize faceImgSize = new CvSize();

		nEigens = nTrainFaces - 1;

		faceImgSize.width(50);
		faceImgSize.height(50);

		eigenVects = new ArrayList<IplImage>(nEigens);

		for (i = 0; i < nEigens; i++) {
			eigenVects.add(cvCreateImage(faceImgSize, IPL_DEPTH_32F, 1));
		}

		eigenValMat = cvCreateMat(1, nEigens, CV_32FC1);

		avgTrainImg = cvCreateImage(faceImgSize, IPL_DEPTH_32F, 1);

		calcLimit = cvTermCriteria(CV_TERMCRIT_ITER, nEigens, 1);

		cvCalcEigenObjects(nTrainFaces, convertListToArray(faceImages4Train),
				convertListToArray(eigenVects), CV_EIGOBJ_NO_CALLBACK, 0, null,
				calcLimit, avgTrainImg, eigenValMat.data_fl());

		LOGGER.info("normalizing the eigenvectors");
		cvNormalize(eigenValMat, eigenValMat, 1, 0, CV_L1, null);
	}

	private IplImage[] convertListToArray(List<IplImage> images) {
		IplImage[] iplImages = new IplImage[images.size()];
		for (int i = 0; i < images.size(); i++) {
			iplImages[i] = images.get(i);
		}
		return iplImages;
	}

	private void storeTrainedData() {
		CvFileStorage fileStorage;

		String trainedDateFilePath = path + partition.getName() + "/" + RESOURCE_TRAINED_DATA;
		File trainedDateFile = new File(trainedDateFilePath);
		if (trainedDateFile.exists()) {
			trainedDateFile.delete();
		}
		try {
			trainedDateFile.createNewFile();
			FileOutputStream out = new FileOutputStream(trainedDateFile);
			out.write("<?xml version=\"1.0\"?>\r\n".getBytes());
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		fileStorage = cvOpenFileStorage(trainedDateFilePath, null, CV_STORAGE_WRITE, null);

		cvWriteInt(fileStorage, "nPersons", personNumber);

		for (int i = 0; i < personNumber; i++) {
			String varname = "personName_" + (i + 1);
			cvWriteString(fileStorage, varname, personNames.get(i), 0);
		}

		cvWriteInt(fileStorage, "nEigens", nEigens);

		cvWriteInt(fileStorage, "nTrainFaces", nTrainFaces);

		cvWrite(fileStorage, "trainPersonNumMat", personNumTruthMat);

		cvWrite(fileStorage, "eigenValMat", eigenValMat);

		cvWrite(fileStorage, "projectedTrainFaceMat", projectedTrainFaceMat);

		cvWrite(fileStorage, "avgTrainImg", avgTrainImg);

		for (int i = 0; i < nEigens; i++) {
			String varname = "eigenVect_" + i;
			cvWrite(fileStorage, varname, eigenVects.get(i));
		}

		cvReleaseFileStorage(fileStorage);
	}

	private void storeEigenfaceImages() {
		cvSaveImage(path + partition.getName() + "/out_averageImage.jpg", avgTrainImg);

		if (nEigens > 0) {
			int COLUMNS = 8;
			int nCols = Math.min(nEigens, COLUMNS);
			int nRows = 1 + (nEigens / COLUMNS);
			int w = eigenVects.get(0).width();
			int h = eigenVects.get(0).height();
			CvSize size = cvSize(nCols * w, nRows * h);
			final IplImage bigImg = cvCreateImage(size, IPL_DEPTH_8U, 1);
			for (int i = 0; i < nEigens; i++) {
				IplImage byteImg = convertFloatImageToUcharImage(eigenVects.get(i));
				int x = w * (i % COLUMNS);
				int y = h * (i / COLUMNS);
				CvRect ROI = cvRect(x, y, w, h);
				cvSetImageROI(bigImg, ROI);
				cvCopy(byteImg, bigImg, null);
				cvResetImageROI(bigImg);
				cvReleaseImage(byteImg);
			}
			cvSaveImage(path + partition.getName() + "/out_eigenfaces.jpg", bigImg);
			cvReleaseImage(bigImg);
		}
	}

	private IplImage convertFloatImageToUcharImage(IplImage srcImg) {
		IplImage dstImg;
		if ((srcImg != null) && (srcImg.width() > 0 && srcImg.height() > 0)) {
			CvPoint minloc = new CvPoint();
			CvPoint maxloc = new CvPoint();
			double[] minVal = new double[1];
			DoublePointer minValue = new DoublePointer(minVal);
			double[] maxVal = new double[1];
			DoublePointer maxValue = new DoublePointer(maxVal);
			cvMinMaxLoc(srcImg, minValue, maxValue, minloc, maxloc, null);
			if (minVal[0] < -1e30) {
				minVal[0] = -1e30;
			}
			if (maxVal[0] > 1e30) {
				maxVal[0] = 1e30;
			}
			if (maxVal[0] - minVal[0] == 0.0f) {
				maxVal[0] = minVal[0] + 0.001;
			}
			dstImg = cvCreateImage(cvSize(srcImg.width(), srcImg.height()), 8, 1);
			cvConvertScale(srcImg, dstImg, 255.0 / (maxVal[0] - minVal[0]), -minVal[0] * 255.0 / (maxVal[0] - minVal[0]));
			return dstImg;
		}
		return null;
	}

}
