package com.zyeeda.facedaemon.util;

import java.io.File;
import java.io.IOException;

import com.zyeeda.facedaemon.Constants;
import com.zyeeda.facedaemon.FacedaemonRuntimeExceptioin;

/**
 * 文件操作辅助类
 *
 * TODO 去掉
 * 
 * @author child
 * @date 2014年11月26日
 *
 */
public class FileHelper {
	
	/**
	 * 创建文件，如果文件所在的路径不存在创建路径后再创建文件
	 *
	 * @param filePath
	 * @return File
	 * @throws IOException
	 * 
	 * @author child
	 * @date 2014年11月26日
	 *
	 */
    public static File createFile(String filePath){  
        File file = new File(filePath);
        if(! file.exists()) {  
            makeDir(file.getParentFile());  
        }  
        try {
			file.createNewFile();
		} catch (IOException e) {
			throw new FacedaemonRuntimeExceptioin("create file '" + file + "' fail.");
		}
        return file;
    }
    
    /**
     * 创建文件夹，如果文件夹所在的路径不存在创建路径后再创建文件夹
     *
     * @param dir
     *
     * void
     *
     * @author child
     * @date 2014年11月26日
     *
     */
    public static void makeDir(File dir) {  
        if(! dir.getParentFile().exists()) {  
            makeDir(dir.getParentFile());  
        }
        if(!dir.exists()){
        	dir.mkdir();  
        }
    }
    
    /**
     * 根据给定的图片名称，获取其 base64 文件
     *
     * @param imageFile
     * @return File
     *
     * @author child
     * @date 2014年11月26日
     *
     */
    public static File getBase64ImageFile(File imageFile){
    	File file = new File(imageFile.getPath() + Constants.EXTENSION_NAME_BASE64);
    	return file;
    }
}
