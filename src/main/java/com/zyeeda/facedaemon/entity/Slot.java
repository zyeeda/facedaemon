package com.zyeeda.facedaemon.entity;

import java.util.List;

public class Slot<T extends Image> {

	private String name;
	private List<T> items;
	private Partition partition;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<T> getItems() {
		return items;
	}
	public void setItems(List<T> items) {
		this.items = items;
	}
	public Partition getPartition() {
		return partition;
	}
	public void setPartition(Partition partition) {
		this.partition = partition;
	}
	
}
