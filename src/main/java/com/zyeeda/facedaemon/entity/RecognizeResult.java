package com.zyeeda.facedaemon.entity;

public class RecognizeResult {
	private String name;
	private Float confidence;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Float getConfidence() {
		return confidence;
	}
	public void setConfidence(Float confidence) {
		this.confidence = confidence;
	}
}
