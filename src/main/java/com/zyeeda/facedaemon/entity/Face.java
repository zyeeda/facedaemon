package com.zyeeda.facedaemon.entity;

import java.io.File;

import com.zyeeda.facedaemon.util.FileHelper;

public class Face extends Image{
	
	
	private String fileBase64;
	
	private Integer slotOrder;
	
	public Integer getSlotOrder() {
		return slotOrder;
	}
	public void setSlotOrder(Integer slotOrder) {
		this.slotOrder = slotOrder;
	}
	public String getFileBase64() {
		return fileBase64;
	}
	public void setFileBase64(String fileBase64) {
		this.fileBase64 = fileBase64;
	}
	@Override
	public void renameTo(File dest) {
		getFile().renameTo(dest);
		FileHelper.getBase64ImageFile(getFile()).renameTo(FileHelper.getBase64ImageFile(dest));
	}
	@Override
	public void delete() {
		getFile().delete();
		FileHelper.getBase64ImageFile(getFile()).delete();
	}

}
