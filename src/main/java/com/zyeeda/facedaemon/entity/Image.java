package com.zyeeda.facedaemon.entity;

import java.io.File;

import org.bytedeco.javacpp.opencv_core.IplImage;

public abstract class Image {
	
	private String name;
	
	private Slot<? extends Image> slot;
	
	private IplImage image;
	
	private File file;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public IplImage getImage() {
		return image;
	}

	public void setImage(IplImage image) {
		this.image = image;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}
	
	public Slot<? extends Image> getSlot() {
		return slot;
	}
	public void setSlot(Slot<? extends Image> slot) {
		this.slot = slot;
	}
	
	
	public abstract void renameTo(File dest);
	
	public abstract void delete();
	
}
