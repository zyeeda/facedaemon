package com.zyeeda.facedaemon.upload;

import javax.servlet.ServletException;

import com.zyeeda.facedaemon.Facedaemon;
import com.zyeeda.facedaemon.entity.Camera;
import com.zyeeda.facedaemon.entity.Partition;

public class TestUploadServlet extends AbstractUploadServlet {

    private static final long serialVersionUID = -4039415130519419316L;
    
	@Override
	public void init() throws ServletException {
		
		facedaemon = new Facedaemon();
		facedaemon.setRepositoryPath("/Users/child/Documents/tmp/pictures/");
		
        Camera c = new Camera();
        c.setSn("zyeeda");
        
        Partition p = new Partition();
        p.setName("zyeeda");
        
        facedaemon.getEngineService().associateCamera(c, p);
        facedaemon.getEngineService().startProcessing();
        
        super.init();
	}
    

}
