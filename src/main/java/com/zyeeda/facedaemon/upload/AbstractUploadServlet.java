package com.zyeeda.facedaemon.upload;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import com.zyeeda.facedaemon.Constants;
import com.zyeeda.facedaemon.Facedaemon;
import com.zyeeda.facedaemon.entity.Camera;
import com.zyeeda.facedaemon.entity.Partition;
import com.zyeeda.facedaemon.entity.Photo;
import com.zyeeda.facedaemon.entity.Slot;

public abstract class AbstractUploadServlet extends HttpServlet {

    private static final long serialVersionUID = -4039415130519419316L;
    
    protected Facedaemon facedaemon; 
    
    private File tempDir;

//    private ServletFileUpload fileUpload = new ServletFileUpload(new DiskFileItemFactory());
    
	@Override
	public void init() throws ServletException {
		super.init();
		tempDir = new File(Facedaemon.getRepositoryPath() + Constants.REPOSITORY_NAME_ORIGIN +"temp");
	}
    
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        try {
        	ServletFileUpload fileUpload = new ServletFileUpload(new DiskFileItemFactory());
            List<FileItem> items = fileUpload.parseRequest(request);
            
            String fileName = null;
            InputStream fileContent = null;
            Partition partition = null;
            Camera camera = null;
            
            File photoFile = null;
            FileOutputStream fos = null;
            Photo photo = null;
            String sn = request.getParameter("sn");
            Slot<Photo> slot = null;
            
            photo = new Photo();
            
            camera = new Camera();
            camera.setSn(sn);
            
            partition = facedaemon.getEngineService().findPartitionByCameraSn(sn);
            
            slot = new Slot<Photo>();
            slot.setName(sn);
            slot.setPartition(partition);
            
            for (FileItem item : items) {
            	if(!item.isFormField()){
                    fileName = FilenameUtils.getName(item.getName());
                    fileContent = item.getInputStream();
                    photoFile = new File(tempDir.getPath()+ "/" + fileName );
                    fos = new FileOutputStream(photoFile);
                    IOUtils.copy(fileContent, fos);
                    fos.close();
                    
                    photo.setCamera(camera);
                    photo.setName(fileName);
                    photo.setFile(photoFile);

                    facedaemon.getTransitRepository().addItemToSlot(photo, slot);
                }
            }
            
        } catch (FileUploadException e) {
            e.printStackTrace();
            response.getWriter().write("error");
        }
        response.getWriter().write("ok");
    }

}
